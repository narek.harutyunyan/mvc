<section>

    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="profile">
                    <h1 class="page-header"><?= $name ?></h1>
                    <div class="row">
                        <div class="col-md-4">
                            <?php if($gender==='Male'){ ?>
                            <img src="<?= App::url('Images/user.png') ?>" class="img-thumbnail" alt="">
                            <?php } elseif ($gender==='Female'){?>
                            <img src="<?= App::url('Images/femaleimg.png')  ?>" class="img-thumbnail" alt="">
                            <?php } ?>
                        </div>
                        <div class="col-md-8">
                            <ul>
                                <li class="info"><strong>Name:</strong><?= $name ?></li>
                                <li class="info"><strong>Email:</strong><?= $email ?></li>
                                <li class="info"><strong>Gender:</strong><?= $gender ?></li>
                                <li class="info"><strong>Age:</strong><?= $age ?></li>
                                <a class="btn btn-primary" id="editProfile" href="#">Edit Profile</a>

                            </ul>
                        </div>
                    </div><br><br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Profile Wall</h3>
                                </div>
                                <div class="panel-body">
                                    <form>
                                        <div class="form-group">
                                            <textarea class="form-control" placeholder="Write on the wall"></textarea>
                                        </div>
                                        <button type="submit" class="btn btn-default">Submit</button>
                                        <div class="pull-right">
                                            <div class="btn-toolbar">
                                                <button type="button" class="btn btn-default"><i class="fa fa-pencil"></i>Text</button>
                                                <button type="button" class="btn btn-default"><i class="fa fa-file-image-o"></i>Image</button>
                                                <button type="button" class="btn btn-default"><i class="fa fa-file-video-o"></i>Video</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default friends">
                    <div class="panel-heading">
                        <h3 class="panel-title">My Friends</h3>
                    </div>
                    <div class="panel-body">
                        <ul>
                            <li><a href="profile.html" class="thumbnail"><img src="<?= App::url('Images/user.png') ?>" alt=""></a></li>
                            <li><a href="profile.html" class="thumbnail"><img src="<?= App::url('Images/user.png') ?>" alt=""></a></li>
                            <li><a href="profile.html" class="thumbnail"><img src="<?= App::url('Images/user.png') ?>" alt=""></a></li>
                            <li><a href="profile.html" class="thumbnail"><img src="<?= App::url('Images/user.png') ?>" alt=""></a></li>
                            <li><a href="profile.html" class="thumbnail"><img src="<?= App::url('Images/user.png') ?>" alt=""></a></li>
                            <li><a href="profile.html" class="thumbnail"><img src="<?= App::url('Images/user.png') ?>" alt=""></a></li>

                        </ul>
                        <div class="clearfix"></div>
                        <a class="btn btn-primary" href="#">View All Friends</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>