<div class="login-page">
    <div class="form">

        <form class="login-form" method="post" action="<?= App::url('index/login') ?>">
            <?php
            if(App::session()->get('success')){
                echo "<div class='alert alert-success'  style='margin-top: 23px;width: 94.5%'>";
                echo "<strong>Success!</strong>  ".App::session()->get('success')."<br>";
                echo "</div>";
            }
            App::session()->delete('success');

            ?>
            <h1 style="color: black">Login</h1> <br>
            <?php
            if(App::session()->get('email')){
                echo "<div class='alert alert-danger'  style='margin-top: 23px;width: 94.5%'>";
                echo "<strong>Warning!</strong>  ".App::session()->get('email')."<br>";
                echo "</div>";
            }
            App::session()->delete('email');

            ?>
            <input type="text" placeholder="Email" name='email' style="   background-color: darkgray;"/>
            <?php
            if(App::session()->get('pwd')){
                echo "<div class='alert alert-danger'  style='margin-top: 23px;width: 94.5%'>";
                echo "<strong>Warning!</strong>  ".App::session()->get('pwd')."<br>";
                echo "</div>";
            }
            App::session()->delete('pwd');

            ?>
            <input type="password" placeholder="password" name='pwd' style="  background-color: darkgray;"/>
            <input type="submit" value="Login" name="login" style="background-color: #43A047">
            <p class="message">Not registered? <a href="<?= App::url('index/index') ?>">Create an account</a></p>
        </form>
    </div>
</div>

