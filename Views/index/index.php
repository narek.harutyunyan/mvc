
<!-- main -->
<div class="main-w3layouts wrapper">





    <div class="main-agileinfo">



        <div class="agileits-top" style="background-color: white">
            <h1 style="color: black;margin-bottom: 20px" >Registration</h1>
            <form action="<?= App::url('index/registration') ?>" method="post">



                <input class="text" type="text" name="firstname" placeholder="Name" style="background-color: #a9a9a9;"  >
                <?php
                if(App::session()->get('name')){
                    echo "<div class='alert alert-danger'  style='margin-top: 23px;width: 94.5%'>";
                    echo "<strong>Warning!</strong>  ".App::session()->get('name')."<br>";
                    echo "</div>";
                }
                App::session()->delete('name');

                ?>

                <input class="text" type="text" name="lastname" placeholder="Lastname"  style="margin-top: 25px;background-color: #a9a9a9;">
                <?php
                if(App::session()->get('lastname')){
                    echo "<div class='alert alert-danger'  style='margin-top: 23px;width: 94.5%'>";
                    echo "<strong>Warning!</strong>  ".App::session()->get('lastname')."<br>";
                    echo "</div>";
                }
                App::session()->delete('lastname');

                ?>

                <input class="text email" type="text" name="email" placeholder="Email" style="background-color: #a9a9a9;" >
                <?php
                if(App::session()->get('email')){
                    echo "<div class='alert alert-danger'  style='margin-top: 23px;width: 94.5%'>";
                    echo "<strong>Warning!</strong>  ".App::session()->get('email')."<br>";
                    echo "</div>";

                }
                App::session()->delete('email');

                ?>

                <label style="color: black">Male <input type="radio" value="Male" name="gender" style="background-color: #a9a9a9;"></label>
                <label style="color: black">Female <input type="radio" value="Female" name="gender" style="background-color: #a9a9a9;"></label> <br>
                <?php
                if(App::session()->get('gender')){
                    echo "<div class='alert alert-danger'  style='margin-top: 23px;width: 94.5%'>";
                    echo "<strong>Warning!</strong>  ".App::session()->get('gender')."<br>";
                    echo "</div>";

                }
                App::session()->delete('gender');

                ?>

              <input placeholder="Date" autocomplete="off" type="text" id="datepicker" name="date" style="margin-top: 25px;background-color: #a9a9a9;">
                <?php
                if(App::session()->get('age')){
                    echo "<div class='alert alert-danger'  style='margin-top: 23px;width: 94.5%'>";
                    echo "<strong>Warning!</strong>  ".App::session()->get('age')."  Or are you really 0 years old?"."<br>";
                    echo "</div>";

                }
                App::session()->delete('age');

                ?>

                <input class="text" type="password" name="password" placeholder="Password"  style="margin-top: 25px;background-color: #a9a9a9;">

                <input class="text w3lpass" type="password" name="cpassword" placeholder="Confirm Password" style="background-color: #a9a9a9;">
                <?php
                if(App::session()->get('pwd')){
                    echo "<div class='alert alert-danger'  style='margin-top: 23px;width: 94.5%'>";
                    echo "<strong>Warning!</strong>  ".App::session()->get('pwd')."<br>";
                    echo "</div>";

                }
                App::session()->delete('pwd');

                ?>
                <input type="submit" name="reg" value="signup">
                <p class="message" style="color: black">Login? <a style="color: #83b96c" href="<?= App::url('index/login') ?>">Login account</a></p>

            </form>

        </div>
    </div>


</div>
<!-- //main -->
