

<br>
<script src="https://use.fontawesome.com/45e03a14ce.js"></script>


<div class="main_section">
    <div class="container">
        <div class="chat_container">
            <div class="col-sm-3 chat_sidebar">
                <div class="row">

                    <div class="dropdown all_conversation">
                        <button class="dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-weixin" aria-hidden="true"></i>
                            All Users
                            <span class="caret pull-right"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                            <li><a href="#"> All Conversation </a>  <ul class="sub_menu_ list-unstyled">
                                    <li><a href="#"> All Conversation </a> </li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li><a href="#">Separated link</a></li>
                        </ul>
                    </div>
                    <div class="member_list">
                        <ul class="list-unstyled">
                            <?php
                                foreach ($users as $user){
                                    if($user['id']===App::session()->get('userId')){

                                    }else{ ?>
                                        <li class="left clearfix user" id="<?= $user['id'] ?>">

                     <span class="chat-img pull-left">
                     <img src="<?= App::url('Images/user.png') ?>" alt="User Avatar" class="img-circle">
                   </span>
                                            <div class="chat-body clearfix">
                                                <div class="header_sec">
                                                    <strong class="primary-font nameuser"><?= $user['name']." ".$user['lastname'] ?></strong> <strong class="pull-right">
                                                    </strong>
                                                </div>
                                                <div class="contact_sec">
                                                    <strong class="primary-font"></strong> <span class="badge pull-right" id="newmsg.<?=$user['id'] ?>"></span>
                                                </div>
                                            </div>
                                        </li>


                            <?php } }?>







                        </ul>
                    </div></div>
            </div>
            <!--chat_sidebar-->


            <div class="col-sm-9 message_section">
                <div class="row">
                    <div class="new_message_head">
                        <div class="pull-left"><button > <h4 id="qwerty">Message</h4></button></div><div class="pull-right"><div class="dropdown">


                            </div></div>
                    </div><!--new_message_head-->

                    <div class="chat_area">

                    </div><!--chat_area-->


                    <div class="message_write">
                        <textarea class="message form-control" placeholder="type a message"></textarea>
                        <div class="clearfix"></div>

                        <button  class="pull-right btn btn-success send">
                            Send</button></div>
                </div>
            </div>
        </div> <!--message_section-->
    </div>
</div>
</div>

<script>
   window.onload=function () {


           document.querySelector('.send').addEventListener('click',function () {
               let msg=document.querySelector('.message').value;
               let id=this.getAttribute('id');
               console.log(msg,id)
               $.ajax({
                   url:'/messages/send/',
                   type:'post',
                   data:{
                       "msg":msg,
                       "id":id,
                   },
                   success:function () {
                       document.querySelector('.message').value="";
                       console.log("sending")
                   },

               })
           });




       let users=document.querySelectorAll('.user');
       for (let i = 0; i <users.length ; i++) {
           users[i].addEventListener('click',function (e) {
               e.preventDefault();
               let id=this.getAttribute('id');
               document.querySelector('.send').setAttribute('id',id);
               let a=window.globalVar;
               clearInterval(a);


               window.globalVar= setInterval(function () {
                   $.ajax({
                       url:'/messages/show/'+id,
                       type:'get',

                       success: function(arra){
                           let array=JSON.parse(arra);
                           let html="<ul class='list-unstyled msg'></ul>"
                           $(".chat_area").html(html);
                           let x;
                           for (i=0;i<array.length;i++){
                               if(array[i]['status']=="0"){
                                   x='unread';
                               }else{
                                   x='read';
                               }
                               if(id==array[i]['userto']){
                                   var l= "<li  class='left clearfix'>"+
                                       " <span class='chat-img1 pull-left'>"+

                                       "</span>"+
                                       "<div class='chat-body1 clearfix'>"+
                                       "<p style='background-color: #d9534f;color:white;border-radius: 10px'>"+array[i]['message']+"</p>"+
                                       "<div style='background-color: #d9534f;color:white;border-radius: 5px' class='chat_time pull-right'>"+x+" "+array[i]['created_at']+"</div>"+
                                       "</div>"+
                                       "</li>";
                                   $('.msg').append(l);
                               }else{
                                   var li= "<li class='left clearfix admin_chat'>"+
                                       " <span class='chat-img1 pull-right'>"+

                                       "</span>"+
                                       "<div class='chat-body1 clearfix'>"+
                                       "<p style='background-color: #428bca;color:white;border-radius: 10px'>"+array[i]['message']+"</p>"+
                                       "<div style='background-color: #428bca;color:white;border-radius: 5px' class='chat_time pull-left'>"+array[i]['created_at']+"</div>"+
                                       "</div>"+
                                       "</li>";
                                   $('.msg').append(li).scrollTop($('.msg').prop('scrollHeight'));;
                               }
                           }

                           var div = $(".chat_area");
                           div.scrollTop(div.prop('scrollHeight'));

                       }

                   })
               },1000);

               console.log(id)
           })
       }
       
   }
</script>

