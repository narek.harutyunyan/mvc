
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dobble Social Network: Profile Page</title>
    <script defer src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <script defer type="text/javascript" src="<?= App::url('Js/script.js') ?>"></script>

    <link href="<?= App::url('Css/bootstrap.css') ?>" rel="stylesheet">
    <link href="<?= App::url('Css/style.css') ?>" rel="stylesheet">
    <link href="<?= App::url('Css/styleprofile.css') ?>" rel="stylesheet">
    <link href="<?= App::url('Css/font-awesome.css') ?>" rel="stylesheet">
</head>

<body>

<header>
    <div class="container">
        <h1>Social Network</h1>
    </div>
</header>

<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="<?= App::url('home/index') ?>">Home</a></li>

                <li><a href="members.html">Members</a></li>

                <li><a href="photos.html">Photos</a></li>
                <li><a href="profile.html">Profile</a></li>
                <li ><a href="<?= App::url('messages/index') ?>">Messages</a></li>
                <li><a name="logout" href="<?= App::url('home/logout') ?>" >Logout</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<?=$content ?>

</body>
</html>