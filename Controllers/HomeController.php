<?php
class HomeController extends ExtendsController {

    public function indexAction(){

            if (!App::session()->get('userId')){
                $this->redirect('index/login');
            }
            $this->layout='profile';

            $userid=App::session()->get('userId');
            $users=new Users();
            $user=$users->select()->where(['id'=>$userid])->one();

            $this->render('index',[
                "name"=>$user['name']." ".$user['lastname'],
                'email'=>$user['email'],
                'gender'=>$user['gender'],
                'age'=>$user['age']
            ]);
    }

    public function LogoutAction(){
        App::session()->destroy();
        $this->redirect('index/login');
    }

}