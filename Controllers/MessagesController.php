<?php

class MessagesController extends ExtendsController{

    public function indexAction(){

        if (!App::session()->get('userId')){
            $this->redirect('index/login');
        }
        $this->layout='profile';
        $user=new Users();
        $users=$user->select()->all();
        $this->render('index',[
            "users"=>$users
        ]);
    }

    public function sendAction(){

        $msg=new Message();
        $data=[
            "userfrom"=>App::session()->get('userId'),
            "userto"=>$_POST['id'],
            'message'=>$_POST['msg'],

        ];

        $msg->insert($data);


    }
    public function showAction(){
        $messages=new Message();
        $msg=$messages->select()->where(['userfrom'=>App::session()->get('userId'),'userto'=>$_GET['id']])->all();
        $send=$messages->select()->where(['userfrom'=>$_GET['id'],'userto'=>App::session()->get('userId')])->all();

        $array = array_merge($msg, $send);
        usort($array, function($a, $b) {
            return strtotime($a['created_at']) - strtotime($b['created_at']);
        });

        echo  json_encode($array ,JSON_PRETTY_PRINT);



    }




}
