<?php

class IndexController extends ExtendsController{

    public function IndexAction(){


        if (App::session()->get('userId')){
            $this->redirect('home/index');
        }

       return $this->render('index');
    }

    public function RegistrationAction(){



        if(App::request()->post('reg')){

            $firstname=App::request()->post('firstname');
            $lastname=App::request()->post('lastname');
            $email=App::request()->post('email');
            $gender=App::request()->post('gender');
            $date=App::request()->post('date');
            $password=App::request()->post('password');
            $cpassword=App::request()->post('cpassword');
            
            if($date){
                $birthday_timestamp = strtotime($date);
                $age = date('Y') - date('Y', $birthday_timestamp);
                if (date('md', $birthday_timestamp) > date('md')) {
                    $age--;
                }

            }else{
                $age=null;
            }



            if($password===$cpassword && !empty($password) && !empty($cpassword) ){

                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    App::session()->set('email',"Email is not valid!!");

                }

                $data=[
                    "name"=>$firstname,
                    "lastname"=>$lastname,
                    "email"=>$email,
                    "gender"=>$gender,
                    'age'=>$age,
                    "password"=>password_hash("$password", PASSWORD_DEFAULT),
                ];
                if (!preg_match("/^[ЁА-яA-z]+$/",$firstname))
                {
                    App::session()->set('name',"Wrote a number or characters!!");
                }
                if (!preg_match("/^[ЁА-яA-z]+$/",$lastname))
                {
                    App::session()->set('lastname',"Wrote a number or characters!!");
                }
                if(strlen($password)<6){
                    App::session()->set('pwd',"Write more than 6 characters!!");
                }

                $isempty=[];

                foreach ($data as $key=>$val){
                    if(empty($data[$key])){
                        $isempty[]=$key;
                    }
                }

                if(!empty($isempty)){
                    for ($i=0;$i<count($isempty);$i++){

                        App::session()->set("$isempty[$i]","Empty $isempty[$i] Field");
                    }

                    $this->redirect('index/index');
                }else{
                    $user=new Users();
                    $addUser=$user->insert($data);
                    App::session()->set('success',"You have successfully registered!!");
                    if($addUser){
                        $this->redirect('index/login');
                    }else{
                        echo "chi gnacel";die;
                    }

                }



            }else{
                App::session()->set("pwd","Passwords do not match");

                $this->redirect('index/index');
            }


        }



    }
    public function LoginAction(){
        if (App::session()->get('userId')){
            $this->redirect('home/index');
        }

        if(App::request()->post('login')){

            $email=App::request()->post('email');
            $pwd=App::request()->post('pwd');
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                App::session()->set('email',"Email is not valid!!");
            }elseif (!empty($email)){
                if(strlen($pwd)>6){
                    if (!preg_match("/^[ЁА-яA-z0-9]+$/",$pwd))
                    {
                         App::session()->set('pwd',"Password must be only letters and numbers!!");
                    }else{
                        $users= new Users();
                        $user=$users->select()->where([
                            'email'=>$email,
                        ])->one();



                        if (password_verify($pwd, $user['password'])) {

                            App::session()->set('userId', $user["id"]);
                            if($user['status']==="user"){
                                $this->redirect('home/index');
                            }else if($user['status']==="admin"){
                                $this->redirect('admin/index');
                            }


                        } else {

                            App::session()->set('pwd','Invalid password.!!');
                        }


                    }
                }else{
                     App::session()->set('pwd',"Write more than 6 characters!!");
                }
            }else{
                 App::session()->set('email',"Email field is empty!!");
            }

        }
        $this->render('login');
    }


}