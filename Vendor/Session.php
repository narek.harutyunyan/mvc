<?php

class Session{

    public static $instance=null;

    public function  __construct()
    {
        session_start();
    }

    public static function getInstance(){
        if(!self::$instance){
            self::$instance=new self();
        }
        return self::$instance;
    }

    public function set($key,$value){
        $_SESSION[$key]=$value;
    }

    public function get($key = null){
        if(!$key){
            return$_SESSION;
        }
        if(isset($_SESSION[$key])){
            return $_SESSION[$key];
        }
        return false;
    }
    public function delete($key){
        if(isset($_SESSION[$key])){
             unset($_SESSION[$key]);
        }
    }
    public function destroy(){
       return session_destroy();
    }


}