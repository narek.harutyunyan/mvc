<?php

class Model{

    private $connection;
    public $sql;
    public $ne=false;
    public $table = null;
    private static $instance;


    public function __construct()
    {

        $this->connect();

        if(!$this->table){

            $this->table=lcfirst(get_class($this));
        }
    }

    public function connect(){
        $config=parse_ini_file(CONFIG.'db.ini');
        $this->connection=new mysqli($config['hosts'],$config['username'],$config['password'],$config['dbname']);

    }


    public static function getInstance(){

        if(!self::$instance){
            self::$instance=new self();
        }
        return self::$instance;

    }

    public function insert($data){
        $keyArray=[];
        $valueArray=[];
        foreach ($data as $key=>$value){
            $keyArray[]="`".$key."`";
            $valueArray[]="'".$value."'";
        }
        $keys=implode(',',$keyArray);
        $vals=implode(',',$valueArray);
        $this->sql="INSERT INTO `$this->table` ($keys) VALUES ($vals)";
        return $this->query($this->sql);
    }
    public function select($cols='*'){

        $colsArray=[];
        if(is_array($cols)){
            foreach ($cols as $key=>$value){
                $colsArray[]="`".$value."`";
            }
            $string=implode(',',$colsArray);
        }else{
            $string=$cols;
        }
        $this->sql="SELECT $string FROM `$this->table`";
        return $this;


    }

    public function where($where=[]){
        $wherearray=[];
        foreach ($where as $key=>$value){
            $wherearray[]="`".$key."`='".$value."'";
        }
        $string=implode('AND',$wherearray);
        $this->sql.=" WHERE $string";
        if ($this->ne){
            return $this->query($this->sql);
        }
        return $this;

    }

    public function update($set,$where){
        $this->ne=true;
        $arrayone=[];
        $arraytwo=[];
        foreach ($set as $key1=>$value1){
            $arrayone[]="`".$key1."`='".$value1."'";
        }
        $data_set=implode(',',$arrayone);
        foreach ($where as $key2=>$value2){
            $arraytwo[]="`".$key2."`='".$value2."'";
        }
        $data_where = implode(' AND ',$arraytwo);
        $sql="UPDATE `$this->table` SET $data_set WHERE $data_where";
        return $this->query($sql);
    }

    public function delete($where){
        $this->ne=true;
        $whereArray=[];
        foreach ($where as $key=>$value){
            $whereArray[]= "`".$key."`='".$value."'";
        }

        $string=implode('AND',$whereArray);
        $sql="DELETE FROM `$this->table` WHERE $string";
        return $this->query($sql);


    }

    public function all(){
        $data=[];
        $result=$this->query($this->sql);
        while ($row = $result->fetch_assoc()){
            $data[] = $row;
        }

        return $data;

    }
    public function one(){
        $result = $this->query($this->sql);
        $row = $result->fetch_assoc();

        return $row;
    }

    public function query($sql){

        return $this->connection->query($sql);


    }







}