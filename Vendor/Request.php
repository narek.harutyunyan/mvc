<?php

class Request{


    public function get($key=false){
        if(!$key){

            $arrGet=[];
            foreach ($_GET as $key=>$value){

                $arrGet[$key]=$this->validate($value);
            }

            return $arrGet;


        }

        if(isset($_GET[$key])){
            return $this->validate($_GET[$key]);
        }

        return false;
    }

    public function post($key=false){
        if(!$key){
            $arrPost=[];

            foreach ($_POST as $key=>$value){
                $arrPost[$key]=$this->validate($value);
            }
            return $arrPost;
        }

        if(isset($_POST[$key])){
            return $this->validate($_POST[$key]);
        }
        return false;

    }

    public function validate($str){
        $str=trim($str);
        $str=htmlspecialchars($str);
        return $str;
    }




}