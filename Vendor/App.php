<?php

include(VENDOR."Model.php");
include (VENDOR.'Session.php');
include(VENDOR.'Request.php');
include(VENDOR."Controller.php");
include(CONTROLLERS.'ExtendsController.php');
Class App
{
    public static $currentController;
    public static $currentAction;
    public static function run(){
        self::loadModels();
        $uri=$_SERVER['REQUEST_URI'];

        $uriArray=explode('/',$uri);
        $controller=DEFAULT_CONTROLLER;
        $action=DEFAULT_ACTION;

        if ($uriArray[1]) {
            $controller=$uriArray[1];
            if($uriArray[2]){
                $action=$uriArray[2];
                if($uriArray[4]){
                    for($i=3;$i<count($uriArray);$i++){
                        if($i%2 != 0){
                            $_GET[$uriArray[$i]]=$uriArray[$i+1];
                        }
                    }
                }
                if(is_numeric($uriArray[3])){
                    $_GET['id']=$uriArray[3];
                }


            }
        }

        self::$currentController=$controller;
        self::$currentAction=$action;




        $actionName=$action."Action";
        $controllerName=ucfirst($controller).'Controller';
        $controllerfileName=$controllerName.".php";

        if(file_exists(CONTROLLERS.$controllerfileName)){

            include(CONTROLLERS.$controllerfileName);
            if(class_exists($controllerName)){
                $controllerObj=new $controllerName;
                if(method_exists($controllerObj,$actionName)){
                    $controllerObj->$actionName();
                }else{
                    throw new Exception("$actionName is not found in $controllerName Class",404);
                }
            }else{
                throw new Exception("$controllerName Class is not found in $controllerfileName ",404);
            }
        }else{
            throw new Exception(" $controllerfileName is not found in Controllers directory",404);
        }

    }


    public static function request(){
        return new Request();
    }

    public static function url($url){
        return "../$url";
    }

    public static function session(){
        return Session::getInstance();
    }

    public function loadModels(){
        foreach (glob(MODELS."*.php") as $models){
            include_once ($models);
        }
    }


}













?>