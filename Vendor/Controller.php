<?php

class Controller {
    public $title="Document";
    public $layout="main";

    public function render($view,$params=[]){

        $viewArray=explode('/',$view);
        extract($params);

        if (count($viewArray) != 1) {
            $viewFIle=$view.'.php';
        } else {
            $viewFIle=App::$currentController.DIRECTORY_SEPARATOR.$view.'.php';
        }
        ob_start();
        include_once (VIEWS.$viewFIle);
        $content=ob_get_clean();

        include_once(LAYOUTS.$this->layout.'.php');



    }

    public function renderPartial($view,$params=[]){
        $viewArray=explode('/',$view);
        extract($params);
        if(count($viewArray)!=1){
            $viewFIle=$view.'.php';
        }else{
            $viewFIle=App::$currentController.DIRECTORY_SEPARATOR.$view.'.php';
        }
        include_once (VIEWS.$viewFIle);
    }


    public function redirect($url){
        header("Location:/$url");
    }

}
